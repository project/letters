<?php


function letters_location_nearby_postalcodes_bylocation($location, $distance, $distance_unit = 'km') {
  // DEBUG: commented code is for testing/debugging purposes
  //$start_time = microtime();

  $latlon = location_latlon_rough($location);
  
  // If we could not get lat/lon coordinates for the given location, return an empty search result set.
  if (!isset($latlon['lat']) || !isset($latlon['lon'])) {
    return array();
  }
  
  // If the distance parameters did not make sense, return an empty search result set.
  if (!($distance_float = _location_convert_distance_to_meters($distance, $distance_unit))) {
    return array();
  }

  $search_results = _letters_location_nearby_postalcodes($latlon['lon'], $latlon['lat'], $distance_float);

  //DEBUG: commented code is for testing/debugging
  //$format_start_time = microtime();
  
  _letters_location_format_search_result_distances($search_results, $distance_unit);
  
  //$format_end_time = microtime();
  //print 'Time for FORMATTING to complete: '. _letters_location_time_difference($format_end_time, $format_start_time) ."<br/>\n";
  
  // DEBUG: commented code is for testing/debugging purposes
  //$end_time = microtime();
  //print 'Time for this search to complete: '. _letters_location_time_difference($end_time, $start_time) ."<br/>\n";
  
  return $search_results;
}


function _letters_location_nearby_postalcodes($lon, $lat, $distance) {
#  $search_results = _letters_location_search_results_from_cache($lon, $lat, $distance);
  
  // If there were usable cached search_results then return those and go no further... awwwww yeah.
#  if (count($search_results)) {
#    return $search_results;
#  }
  //------------------------------------------------------------------------------------------
  // Pulling from the cache takes place right here.
  // The cache id ("cid", to be inserted into the cache table) will be a concatenation of
  //   -> "letters_location_prox_search:".
  //   -> round($lon, 3) . ':'.
  //   -> round($lat, 3) . ":".
  //   -> $distance
  // The value of the cached item will be a serialize($result_array)
  //
  // The cache will be cleared of proximity searches when there is a change in countries that have
  // been configured into the system.  
  //------------------------------------------------------------------------------------------
  
  $search_results = array();
    
  $latrange = earth_latitude_range($lon, $lat, $distance);
  $lonrange = earth_longitude_range($lon, $lat, $distance);  

  //$query_start_time = microtime();
  
  $result = db_query('SELECT zip, city, state, country, '. earth_distance_sql($lon, $lat) .' as distance  FROM {zipcodes} WHERE latitude > %f AND latitude < %f AND longitude > %f AND longitude < %f AND '. earth_distance_sql($lon, $lat) .' < %f ORDER by distance', $latrange[0], $latrange[1], $lonrange[0], $lonrange[1], $distance);
  
  while ($result_row = db_fetch_object($result)) {
    $search_results[$result_row->country . $result_row->zip] = array('city' => $result_row->city, 'province' => $result_row->state, 'distance' => $result_row->distance);
  }
  
  //DEBUG: commented code for testing/debugging purposes
  //$query_end_time = microtime();
  
  //print 'TOTAL TIME FOR _letters_location_nearby_postalcodes() '. _letters_location_time_difference($query_end_time, $query_start_time) ."<br/>\n";
  //--------------------------------------------------------------------------------------------
  // This is the spot where search results are cached
  
  #cache_set('letters_location_prox_search:'. round($lon, 3) .':'. round($lat, 3) .':'. $distance, 'cache', serialize($search_results));
  
  // DEBUG: commented code is for testing/debugging purposes
  //print 'POSTAL CODE SEARCH CACHING: Wrote new search results to cache'."<br/>\n";
  //--------------------------------------------------------------------------------------------
  return $search_results;
}



function _letters_location_search_results_from_cache($lon, $lat, $distance) {
  $cache_id_prefix = 'letters_location_prox_search:'. round($lon, 3) .':'. round($lat, 3) .':';
  
  $result = db_query("SELECT cid FROM {cache} WHERE cid LIKE '%s%%'", $cache_id_prefix);
  
  if ($result_row = db_fetch_object($result)) {
    // A previous search has been done on the same search point, possibily with the an equal or different
    // search radius.
    $cached_key_fields = explode(':', $result_row->cid);
    $previous_search_radius = $cached_key_fields[3];
    
    // If the search-radius is less than or equal to the previous search-radius, then just use
    // the appropriate subset of the previous search result.
    // This is very convenient since previous search results are sorted in ascending order
    // by their distance from the search point.    
    if ($distance <= $previous_search_radius) {

      $cached_search_results = cache_get($result_row->cid);
      $cached_search_results = unserialize($cached_search_results->data);
      // If the cached-search had the exact same search-radius, just return the entire search result's
      // array from before,
      // otherwise, go through the distance-sorted search results and pick them out until the distances 
      // of each search result start being something greater than the current search-radius
      if ($distance == $previous_search_radius) {
        // DEBUG: commented code is for testing/debugging purposes
        //print 'POSTAL CODE SEARCH CACHING: Returning EXACT SAME of search results from before'."<br/>\n";
        return $cached_search_results;
      }
      else {
        $current_search_results = array();
        foreach ($cached_search_results as $key => $cached_result) {
          if ($cached_result['distance'] <= $distance) {
            $current_search_results[$key] = $cached_result;
          }
          else {
            break;
          }
        }
        // DEBUG: commented code is for testing/debugging purposes
        //print 'POSTAL CODE SEARCH CACHING: Returning SUBSET of a previous search\'s results'."<br/>\n";
        return $current_search_results;
      }
    }
    else {
      // If the previous search-radius on the same point is smaller than the current search-radius,
      // then delete the previous search from the cache to make way in the cache for the results of
      // the current, more comprehensive search being done on the same point, but on a larger radius.
      // Return an empty array to let the calling function know that it will have to do a new search.
      
      // DEBUG: commented code is for testing/debugging purposes
      //print 'POSTAL CODE SEARCH CACHING: Throwing out old search on a point because new search uses larger search-radius'."<br/>\n";
      cache_clear_all($result_row->cid, 'cache');
      return array();
    }
    
  }
  else {
    // This else-clause ties back to the first if-clause in this function.
    // It executes if no search has been done on this point.
    // If the {cache} table did not contain any useful cache id's, return the empty array.
    // This will let the calling function know that it has to do an original search.
    return array();
  }
}


function _letters_location_format_search_result_distances(&$results_array, $distance_unit = 'km') {
  if ($distance_unit != 'km' && $distance_unit != 'mile') {
    $distance_unit = 'km';
  }
  
  // $conversion_factor = number to divide by to convert meters to $distance_unit
  // At this point, $distance_unit == 'km' or 'mile' and nothing else
  $conversion_factor = ($distance_unit == 'km') ? 1000.0 : 1609.347;
  
  foreach ($results_array as $index => $single_result) {
    $results_array[$index]['distance'] = round($single_result['distance']/$conversion_factor, 1);
  }
}

